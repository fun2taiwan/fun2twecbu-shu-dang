﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Fun2twEC.Models;
using Fun2twModels.Models;

namespace Fun2twAdmin.Models
{
    public class MailTemplate
    {
        public String MailSubject { get; set; }
        
        public MailTemplate()
        {
            MailSubject = "瘋趣台灣網站系統通知";
        }

        public void SendCancelEmail(Orders orders, ICollection<OrderDetails> ordersOrderDetailses,string email)
        {
            string mailBody = "<html><body><div style=\"color:black\">訂單編號:" + orders.Orderid + "<br/>申請退貨,請與買家聯絡,購買人資料如下:<br/><table border=\"1\"><tr><td style=\"padding:5px;\">姓名</td><td style=\"padding:5px;\">手機</td><td style=\"padding:5px;\">電話</td><td style=\"padding:5px;\">信箱</td></tr><tr><td style=\"padding:5px;\">" + orders.OrderName + "</td><td style=\"padding:5px;\">" +
                              orders.OrderTelphone + "</td><td style=\"padding:5px;\">" + orders.OrderMobile + "</td><td style=\"padding:5px;\">" + orders.OrderEmail+ "</td></tr></table></div></body></html>";

            
           
            String mailFrom = ConfigurationManager.AppSettings["MailFrom"];
            //String mailCc = mailFrom + ";" + "lawfulnatural@gmail.com";


            String mailCc = mailFrom;
            String mailTo = email;
            String mailPassword = ConfigurationManager.AppSettings["MailPassword"];

            Utility.SendGmailMail(mailFrom, mailTo, MailSubject, mailBody, mailPassword, mailCc);
        }




        public void SendNargoCancelEmail(NargoOrders orders, ICollection<NargoOrderDetails> ordersOrderDetailses)
        {
            string mailBody = "<html><body><div style=\"color:black\">訂單編號:" + orders.OrderId + "<br/>申請退貨,請與買家聯絡,購買人資料如下:<br/><table border=\"1\"><tr><td style=\"padding:5px;\">姓名</td><td style=\"padding:5px;\">手機</td><td style=\"padding:5px;\">電話</td><td style=\"padding:5px;\">信箱</td></tr><tr><td style=\"padding:5px;\">" + orders.OrderName + "</td><td style=\"padding:5px;\">" +
                              orders.OrderTelphone + "</td><td style=\"padding:5px;\">" + orders.OrderMobile + "</td><td style=\"padding:5px;\">" + orders.OrderEmail + "</td></tr></table></div></body></html>";




            String mailFrom = ConfigurationManager.AppSettings["MailFrom"];
            //String mailCc = mailFrom + ";" + "lawfulnatural@gmail.com";


            String mailCc = mailFrom;
            String mailTo = "rihsinhuang@gmail.com";
            String mailPassword = ConfigurationManager.AppSettings["MailPassword"];

            Utility.SendGmailMail(mailFrom, mailTo, MailSubject, mailBody, mailPassword, mailCc);
        }


        public void SendOrderEmail(Orders orders, ICollection<OrderDetails> ordersOrderDetailses, string email)
        {
            MailSubject = "商城產品訂購通知信件";
            string mailBody = "<html><body><div style=\"color:black\">訂單編號:" + orders.Orderid + "<br/>訂購資訊如下:<br/><table border=\"1\"><tr>購買人資訊</tr><tr><td style=\"padding:5px;\">購買人姓名</td><td style=\"padding:5px;\">購買人手機</td><td style=\"padding:5px;\">購買人電話</td><td style=\"padding:5px;\">購買人信箱</td><td style=\"padding:5px;\">購買人地址</td></tr><tr><td style=\"padding:5px;\">" + orders.OrderName + "</td><td style=\"padding:5px;\">" +
                              orders.OrderTelphone + "</td><td style=\"padding:5px;\">" + orders.OrderMobile + "</td><td style=\"padding:5px;\">" + orders.OrderEmail + "</td><td style=\"padding:5px;\">" + orders.OrderAddress + "</td></tr><tr>收件人資訊<tr><tr><td style=\"padding:5px;\">收件人姓名</td><td style=\"padding:5px;\">收件人手機</td><td style=\"padding:5px;\">收件人電話</td><td style=\"padding:5px;\">收件人信箱</td><td style=\"padding:5px;\">收件人地址</td></tr><tr><td style=\"padding:5px;\">" + orders.ShippingName + "</td><td style=\"padding:5px;\">" +
                              orders.ShippingTelphone + "</td><td style=\"padding:5px;\">" + orders.ShippingMobile + "</td><td style=\"padding:5px;\">" + orders.ShippingEmail + "</td><td style=\"padding:5px;\">" + orders.ShippingAddress + "</td></tr><tr>購買清單</tr><tr><td style=\"padding:5px;\">品名</td><td style=\"padding:5px;\">單價</td><td style=\"padding:5px;\">數量</td><td style=\"padding:5px;\">金額</td></tr>";
            string ordersOrderDetailsesStr = "";
            foreach (var item in ordersOrderDetailses)
            {
                ordersOrderDetailsesStr += "<tr><td style=\"padding:5px;\">" + item.Subject + "</td>";
                ordersOrderDetailsesStr += "<td style=\"padding:5px;\">" + item.Price + "</td>";
                ordersOrderDetailsesStr += "<td style=\"padding:5px;\">" + item.Quantity + "</td>";
                ordersOrderDetailsesStr += "<td style=\"padding:5px;\">" + item.TotalPrice + "</td></tr>";
            }
            mailBody = mailBody + ordersOrderDetailsesStr +
                       "<tr><td style=\"padding:5px;\">運費</td><td style=\"padding:5px;\">總金額</td></tr><tr><td style=\"padding:5px;\">" +
                       orders.ShippingFee + "</td><td style=\"padding:5px;\">" + orders.Total +
                       "</td></tr></table></div></body></html>";
            String mailFrom = ConfigurationManager.AppSettings["MailFrom"];
            //String mailCc = mailFrom + ";" + "lawfulnatural@gmail.com";


            String mailCc = mailFrom+ ";rihsinhuang@gmail.com" ;
            String mailTo = email;
            String mailPassword = ConfigurationManager.AppSettings["MailPassword"];

            Utility.SendGmailMail(mailFrom, mailTo, MailSubject, mailBody, mailPassword, mailCc);
        }

        public void SendNargoOrderEmail(NargoOrders orders, ICollection<NargoOrderDetails> ordersOrderDetailses)
        {
            MailSubject = "南仁湖產品訂購通知信件";
            string mailBody = "<html><body><div style=\"color:black\">訂單編號:" + orders.OrderId + "<br/>訂購資訊如下:<br/><table border=\"1\"><tr>購買人資訊</tr><tr><td style=\"padding:5px;\">購買人姓名</td><td style=\"padding:5px;\">購買人手機</td><td style=\"padding:5px;\">購買人電話</td><td style=\"padding:5px;\">購買人信箱</td><td style=\"padding:5px;\">購買人地址</td></tr><tr><td style=\"padding:5px;\">" + orders.OrderName + "</td><td style=\"padding:5px;\">" +
                              orders.OrderTelphone + "</td><td style=\"padding:5px;\">" + orders.OrderMobile + "</td><td style=\"padding:5px;\">" + orders.OrderEmail + "</td><td style=\"padding:5px;\">" + orders.OrderAddress + "</td></tr><tr>收件人資訊<tr><tr><td style=\"padding:5px;\">收件人姓名</td><td style=\"padding:5px;\">收件人手機</td><td style=\"padding:5px;\">收件人電話</td><td style=\"padding:5px;\">收件人信箱</td><td style=\"padding:5px;\">收件人地址</td></tr><tr><td style=\"padding:5px;\">" + orders.ShippingName + "</td><td style=\"padding:5px;\">" +
                              orders.ShippingTelphone + "</td><td style=\"padding:5px;\">" + orders.ShippingMobile + "</td><td style=\"padding:5px;\">" + orders.ShippingEmail + "</td><td style=\"padding:5px;\">" + orders.ShippingAddress + "</td></tr><tr>購買清單</tr><tr><td style=\"padding:5px;\">品名</td><td style=\"padding:5px;\">單價</td><td style=\"padding:5px;\">數量</td><td style=\"padding:5px;\">金額</td></tr>";
            string ordersOrderDetailsesStr = "";
            foreach (var item in ordersOrderDetailses)
            {
                ordersOrderDetailsesStr += "<tr><td style=\"padding:5px;\">" + item.Subject + "</td>";
                ordersOrderDetailsesStr += "<td style=\"padding:5px;\">" + item.Price + "</td>";
                ordersOrderDetailsesStr += "<td style=\"padding:5px;\">" + item.Quantity + "</td>";
                ordersOrderDetailsesStr += "<td style=\"padding:5px;\">" + item.TotalPrice + "</td></tr>";
            }
            mailBody = mailBody + ordersOrderDetailsesStr +
                       "<tr><td style=\"padding:5px;\">運費</td><td style=\"padding:5px;\">總金額</td></tr><tr><td style=\"padding:5px;\">" +
                       orders.ShippingFee + "</td><td style=\"padding:5px;\">" + orders.Total +
                       "</td></tr></table></div></body></html>";
            String mailFrom = ConfigurationManager.AppSettings["MailFrom"];
            //String mailCc = mailFrom + ";" + "lawfulnatural@gmail.com";
            String NargoMail = ConfigurationManager.AppSettings["NargoMail"];

            String mailCc = mailFrom+";"+ NargoMail;
            String mailTo = "rihsinhuang@gmail.com";
            String mailPassword = ConfigurationManager.AppSettings["MailPassword"];

            Utility.SendGmailMail(mailFrom, mailTo, MailSubject, mailBody, mailPassword, mailCc);
        }
        public void SendNoProductEmail(string subject, string email)
        {
            string mailBody = "<html><body><div style=\"color:red\">"+subject+"已全數賣完。提醒您盡快補貨，謝謝。</div></body></html>";

            MailSubject = "產品缺貨通知";

            String mailFrom = ConfigurationManager.AppSettings["MailFrom"];
            //String mailCc = mailFrom + ";" + "lawfulnatural@gmail.com";


            String mailCc = mailFrom;
            String mailTo = email;
            String mailPassword = ConfigurationManager.AppSettings["MailPassword"];

            Utility.SendGmailMail(mailFrom, mailTo, MailSubject, mailBody, mailPassword, mailCc);
        }

    }

}