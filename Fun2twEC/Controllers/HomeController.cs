﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fun2twModels.Models;
using MvcPaging;

namespace Fun2twEC.Controllers
{
    public class HomeController : _BaseController
    {
        //
        // GET: /Products/
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;

        public ActionResult Index()
        {
            var products = _db.Products.OrderByDescending(x => x.Counter).Take(4).ToList();
            var mall = _db.Malls.OrderBy(x => x.ListNum).ToList();
            ViewBag.mall = mall;
            return View(products);
        }

        public ActionResult Mall(int? page, FormCollection fc, string orderby, int? classId, int? brandId, int? shopInfoId, int Id = 0)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

            var products = _db.Products.Include(p => p.ProductCategory).Include(p => p.Brand).Include(p => p.ShopInfo).Where(x => x.ProductCategory.MailId == Id && x.UpImageUrl != null).AsQueryable();

            if (classId.HasValue)
            {
                ProductCategory Category =
               _db.ProductCategory.Find(classId);
                ViewBag.Category = Category;
                products = products.Where(w => w.ClassId == classId);
            }
            if (brandId.HasValue)
            {
                products = products.Where(w => w.BrandId == brandId);
            }
            if (shopInfoId.HasValue)
            {

                products = products.Where(w => w.ShopInfoId == shopInfoId);
            }
            if (hasViewData("SearchBySubject"))
            {
                string searchBySubject = getViewDateStr("SearchBySubject");
                products = products.Where(w => w.Subject.Contains(searchBySubject));
            }

            var mall = _db.Malls.Find(Id);
            ViewBag.mall = mall;
            var menu = _db.Menus.FirstOrDefault(x => x.MailId == Id);
            ViewBag.menu = menu;
            ViewBag.orderby = string.IsNullOrEmpty(@orderby) ? "Date" : @orderby;
            if (ViewBag.orderby == "Date")
            {
                products = products.OrderByDescending(p => p.InitDate);
            }
            else
            {
                products = products.OrderByDescending(p => p.Counter);
            }

            var hotProducts = _db.Products.Where(x => x.ProductCategory.MailId == Id).OrderByDescending(x => x.Counter).AsQueryable();
            ViewBag.hotProducts = hotProducts;

            //            ViewBag.Subject = Subject;
            return View(products.ToPagedList(currentPageIndex, DefaultPageSize));
        }



        public ActionResult Details(int id = 0)
        {
            Products products = _db.Products.Find(id);
            if (products == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(products);
        }
    }
}
